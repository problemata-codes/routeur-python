import os
from flask import Flask, redirect, url_for, request, render_template
from werkzeug.utils import secure_filename
import uuid
import json
import shutil
import datetime

ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}

app = Flask(__name__)

# maximum allowed payload to 16 megabytes
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024


@app.route('/')
@app.route('/index')
def index():
    items = getItems()
    return render_template('index.html', items=items)



@app.route('/create', methods = ['POST', 'GET'])
def create():
    if request.method == 'POST':
        name = request.form['name']

        unique_id = uniqid()

        path = "content/" + unique_id
        os.mkdir(path)

        date = datetime.datetime.now()

        json_dict = {
            "created": date,
            "modified": date,
            "name": name,
            "id": unique_id,
            "relative_path": path
        }
        writeJSON(path+"/info.json", json_dict)

        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(path, filename))

        return redirect(url_for('view', item_id = unique_id))
        # abort(401)
    else:
      user = request.args.get('name')
      return render_template('create.html')



@app.route('/view/<item_id>')
def view(item_id):
    item = getItem(item_id)
    return render_template('view.html', item = item)



@app.route('/edit/<item_id>', methods = ['GET', 'POST'])
def edit(item_id):
    item = getItem(item_id)
    if request.method == 'POST':
        
        # modif de du dict de l'item        
        item['name'] = request.form['name']
        item['modified'] = datetime.datetime.now()

        path = item['relative_path']
        writeJSON(path+"/info.json", item)
    return render_template('edit.html', item = item)



@app.route('/delete/<item_id>', methods = ['GET', 'POST'])
def delete(item_id):
    
    deletion = False
    if request.method == 'POST':
        deletion = True

        shutil.rmtree("content/"+item_id)

    return render_template('delete.html', id=item_id, deletion=deletion)



def writeJSON(filepath, json_dict):
    """
    Écrit "json_dict" dans un fichier json vers "filepath"
    """
    json_object = json.dumps(json_dict, indent=4, sort_keys=True, default=str)
    with open(filepath, "w") as outfile: 
        outfile.write(json_object)



def getItem(item_id):
    """
    Retourne un dict
    """
    with open('content/'+item_id+'/info.json', 'r') as openfile: 
        json_object = json.load(openfile)

        # conversion des string dates en objets datetime 
        json_object['created'] = str_to_datetime(json_object["created"])
        json_object['modified'] = str_to_datetime(json_object["modified"])

    return json_object



def getItems():
    """
    - Construit un tableau de tous les items présents dans content/
    - Retourne un tableau de dict trié par ordre croissant de date
    """

    path = 'content/'
    items = []
    for el in os.scandir(path):
        if el.is_dir():
            item = getItem(el.name)
            items.append(item)
    
    # tri par ordre croissant de date
    items.sort(key=lambda item: item["created"])
    
    return items



def uniqid():
    number = uuid.uuid4().int
    alphabet = list("23456789ABCDEFGHJKLMNPQRSTUVWXYZ" "abcdefghijkmnopqrstuvwxyz")
    output = ""
    alpha_len = len(alphabet)
    while number:
        number, digit = divmod(number, alpha_len)
        output += alphabet[digit]
    return output[0:15]



def str_to_datetime(str_date):
    """
    Retourne un objet datetime à partir d'une str
    """
    return datetime.datetime.strptime(str_date, "%Y-%m-%d %H:%M:%S.%f")



def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


# pour l'upload

"""

UPLOAD_FOLDER = 'content/'
ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024


@app.route('/upload')
def upload_file():
   return render_template('upload.html')
    
@app.route('/uploader', methods = ['GET', 'POST'])
def uploader_file():
    if request.method == 'POST':
        f = request.files['file']
        filename = secure_filename(f.filename)
        f.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        return 'file uploaded successfully'

"""



# pour utilisation à partir de `python app.py`

"""
if __name__ == '__main__':
    app.run(debug = True)
"""