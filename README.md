# Routeur Python basé sur Flask

Routeur Python basé sur le framework Flask pour construire des routes, gérer les méthodes HTTP et gérer des templates avec héritage.

Structure :

- `app.py` : déclaration des routes
- `templates/` : dossier des templates .html
- `templates/base.html` : template de base, les autres peuvent en hériter
- `static/` : dossier pour les fichiers statiques
- `content/` : dossier pour stocker les items



## Installation

- création d'un environnement virtuel (testé avec Python^3.9.1) : `python -m venv env/`
- activation de l'environnement : `source env/bin/activate` (pour en sortir : `deactivate`)
- installation des dépendances : `pip install -r requirements.txt`
- vérification avec `pip show flask` par exemple
- ajout des droits d'écriture sur `content/`
- démarrage de l'application :

```
export FLASK_APP=app.py
export FLASK_ENV=development
flask run
```



## Dépendances

- Flask



## Références

- <https://flask.palletsprojects.com/>
- <https://pythonbasics.org/what-is-flask-python/>